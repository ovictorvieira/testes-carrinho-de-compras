<?php
/**
 * Created by PhpStorm.
 * User: viih
 * Date: 24/04/2018
 * Time: 17:32
 */

namespace Victor\Cart\Entities;

use Doctrine\Instantiator\Exception\InvalidArgumentException;

class Product implements ProductInterface {
    protected $name;
    protected $description;
    protected $price;

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        if(!is_numeric($price)) {
            throw new InvalidArgumentException();
        }
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}