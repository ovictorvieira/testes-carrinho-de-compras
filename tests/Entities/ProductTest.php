<?php
/**
 * Created by PhpStorm.
 * User: viih
 * Date: 24/04/2018
 * Time: 17:30
 */

namespace Victor\Cart\Tests\Entities;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Victor\Cart\Entities\Product;
use Victor\Cart\Entities\ProductInterface;

class ProductTest extends \PHPUnit\Framework\TestCase {

    // Teste de quando for criado a interface produto
    public function testProductType() {
    	$product = new Product();
        $this->assertInstanceOf(ProductInterface::class,$product);
    }
    // Teste de quando for passado um Nome para o produto
    public function testProductName() {
        $product = new Product();
        $product->setName("Produto 1");

        $this->assertEquals("Produto 1",$product->getName());
    }
    // Teste de quando for passado uma descrição
    public function testDescriptionValue() {
        $product = new Product();
        $product->setDescription("Descrição do produto");

        $this->assertEquals("Descrição do produto",$product->getDescription());
    }
    // Teste de quando for passado um valor qualquer
    public function testPriceValue() {
        $product = new Product();
        $product->setPrice(10);

        $this->assertEquals(10,$product->getPrice());
    }
    /**
     * @expectedException InvalidArgumentException
     */
    // Teste de quando não for passado um valor númerico
    public function testPriceValueWhenANotNumericGiven() {
        $product = new Product();
        $product->setPrice("valor em string");
    }
}