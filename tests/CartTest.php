<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 24/04/18
 * Time: 19:39
 */

namespace Victor\Cart\Tests;

use Victor\Cart\Entities\Product;
use Victor\Cart\Cart;

class CartTest extends \PHPUnit\Framework\TestCase {
    protected $product;
    protected $product2;
    protected $cart;

    public function setUp()
    {
        $this->product = new Product();
        $this->product2 = new Product();
        $this->cart = new Cart();
    }

    public function testGetProductList() {
        // Cria o Produto 1
        $this->product->setName("Produto 1");
        $this->product->setDescription("Descrição do Produto 2");
        $this->product->setPrice(10);

        // Cria o Produto 2
        $this->product2->setName("Produto 2");
        $this->product2->setDescription("Descrição do Produto 2");
        $this->product2->setPrice(20);

        $this->cart->addProducts($this->product);
        $this->cart->addProducts($this->product2);

        // Cria um array de objetos do tipo produto
        $products = new \ArrayObject([$this->product,$this->product2]);

        // Testa se o que está vindo no método Get é um array de produtos
        $this->assertEquals($products, $this->cart->getProducts());
    }

    public function testGetTotal() {
        // Cria o Produto 1
        $this->product->setName("Produto 1");
        $this->product->setDescription("Descrição do Produto 2");
        $this->product->setPrice(10);

        // Cria o Produto 2
        $this->product2->setName("Produto 2");
        $this->product2->setDescription("Descrição do Produto 2");
        $this->product2->setPrice(20);

        $this->cart->addProducts($this->product);
        $this->cart->addProducts($this->product2);

        $this->assertEquals(30, $this->cart->getTotal());
    }
}